import { Component } from 'react'
import FormikSendEmail from '../components/mail/SendEmailForm'
import Experiment from '../components/Experiment'
import Head from 'next/head'
import Error from './_error'

class index extends Component {

  render() {
      return(
        <div id='index' suppressHydrationWarning={true}>
        <style jsx>{`         
          #index {
            background: linear-gradient(to bottom, rgba(254,255,255,1) 0%,
            rgba(221,241,249,1) 35%,
            rgba(160,216,239,1) 100%);
            height: 100vh;
            margin: auto;
          }     
          `}</style>      
          <Head>
            <title> Mail </title>
          </Head>
          <br/>
          <Experiment />     
        </div>
      )
  }
}


export default index