import React, { Component } from 'react'
import Image from 'semantic-ui-react/dist/commonjs/elements/Image/Image'
import ExperimentErr from '../components/ExperimentErr'
import Label from 'semantic-ui-react/dist/commonjs/elements/Label/Label'
import Link from 'next/link'
export default class Error extends Component {
  render() {
    return (
      <div id='err'>
        <style jsx>{`         
          #err {
            background: linear-gradient(to bottom, rgba(206,220,231,1) 0%,
            rgba(89,106,114,1) 100%);
            height: 100vh;
            display: flex;
            justify-content: center;
            flex-direction: column;
          }
          `}</style>
          <ExperimentErr />
          <Label color='black' size='massive' pointing='below'>404 NOT FOUND. Click the image below.</Label>
          <Link href="/mail" passHref>        
            <Image style={{cursor: 'pointer', margin: 'auto',}}  src='../static/404.jpg' size='massive' rounded centered />
          </Link>
      </div>
    )
  }
}