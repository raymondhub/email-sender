import { Fragment, Component } from 'react'
import FormikSendEmail from '../components/mail/SendEmailForm'
import PaperPlane from '../components/PaperPlane'
import Head from 'next/head'
import Error from './_error'

class mail extends Component {

  render() {
      return(
        <div id='mail' suppressHydrationWarning={true}>
        <style jsx>{`         
          #mail {
            background: linear-gradient(to bottom, 
              rgba(255,255,255,1) 0%,
              rgba(246,246,246,1) 47%,
              rgba(237,237,237,1) 100%);
            height: 118vh;
            margin-top: -50px;
          }     
          `}</style>      
          <Head>
            <title> Mail </title>
            <script src='https://www.google.com/recaptcha/api.js' async defer />
          </Head>
          <br/>
          <PaperPlane />
          <FormikSendEmail />
        </div>
      )
  }
}


export default mail