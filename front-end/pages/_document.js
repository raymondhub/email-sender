// @flow
import Document, { Head, Main, NextScript } from 'next/document'


class MyDocument extends Document {
  render () {
    return (
      <html lang='en' dir='ltr'>
        <Head>
          <link rel='shortcut icon' type='image/x-icon' href='/static/favicon.ico' />
          <meta charSet='utf-8' />
          <meta name='description' content='Email Sending App' />
          <meta name='keywords' content='email' />
          <meta name='robots' content='index, follow' />
          <meta httpEquiv='X-UA-Compatible' content='IE=edge' />
          <meta name='viewport' content={'initial-scale=1, width=device-width'} />
          <style> { /* reset.css starts here */
            `
              'html, body, div, span, object, iframe,\
              h1, h2, h3, h4, h5, h6, p, blockquote, pre,\
              abbr, address, cite, code,\
              del, dfn, em, img, ins, kbd, q, samp,\
              small, strong, sub, sup, var,\
              b, i,\
              dl, dt, dd, ol, ul, li,\
              fieldset, form, label, legend,\
              table, caption, tbody, tfoot, thead, tr, th, td,\
              article, aside, canvas, details, figcaption, figure,\
              footer, header, hgroup, menu, nav, section, summary,\
              time, mark, audio, video': {
                  margin: 0,
                  padding: 0,
                  border: 0,
                  outline: 0,
                  'font-size': '100%',
                  'vertical-align': 'baseline',
                  background: 'transparent',
                  fontFamily: 'Roboto,Arial,Helvetica,sans-serif'
              },
              body: {
                'line-height': 1
            },
              'article, aside, details, figcaption, figure,
              footer, header, hgroup, menu, nav, section': {
                  display: 'block'
              },
              'nav ul': {
                  'list-style': 'none'
              },
              'blockquote, q': {
                  quotes: 'none'
              },
              'blockquote:before, blockquote:after,
              q:before, q:after': {
                  content: '',
                  content: 'none'
              },
              a: {
                  margin: 0,
                  padding: 0,
                  'font-size': '100%',
                  'vertical-align': 'baseline',
                  background: 'transparent',
                  textDecoration: 'none',
              },
              ins: {
                  'background-color': '#ff9',
                  color: '#000',
                  'text-decoration': 'none'
              },
              mark: {
                  'background-color': '#ff9',
                  color: '#000',
                  'font-style': 'italic',
                  'font-weight': 'bold'
              },
              del: {
                  'text-decoration': 'line-through'
              },
              'abbr[title], dfn[title]': {
                  'border-bottom': '1px dotted',
                  'cursor': 'help'
              },
              table: {
                  'border-collapse': 'collapse',
                  'border-spacing': 0
              },
              hr: {
                  display: 'block',
                  height: '1px',
                  border: 0,
                  'border-top': '1px solid #cccccc',
                  margin: '1em 0',
                  padding: 0
              },
              'input, select': {
                  'vertical-align': 'middle'
              },
              html: {
                  WebkitFontSmoothing: 'antialiased',
                  MozOsxFontSmoothing: 'grayscale'
              },`                         
          } /* reset.css ends here */
               
          </style>
          {/*
            manifest.json provides metadata used when your web app is added to the
            homescreen on Android. See https://developers.google.com/web/fundamentals/engage-and-retain/web-app-manifest/
          */}
          <link rel='manifest' href='/static/manifest.json' />
          <link
            rel='stylesheet'
            href='https://fonts.googleapis.com/css?family=Roboto:300,400,500'
          />
          {/* Add react-semantic ui CSS only --- https://github.com/Semantic-Org/Semantic-UI-CSS/releases */}
          <link rel='stylesheet' 
                href='https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.2.14/semantic.min.css' 
          />

        </Head>
        <body>
          <Main />
          <NextScript />
        </body>
      </html>
    )
  }
}

export default MyDocument

