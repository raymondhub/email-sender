module.exports = {
  distDir: 'build', // name  the build 'build instead of the default .next
  webpack (config, { dev }) {
    config.devtool = 'cheap-module-eval-source-map'
    if (!dev) {
      config.devtool = 'source-map'
    }
    return config
  }
}
