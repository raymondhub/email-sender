import { Fragment, PureComponent } from 'react'
import axios from 'axios'
import { withFormik, validateYupSchema } from 'formik'
import Yup from 'yup'
import Message from 'semantic-ui-react/dist/commonjs/collections/Message/Message'
import Form from 'semantic-ui-react/dist/commonjs/collections/Form/Form'
import Button from 'semantic-ui-react/dist/commonjs/elements/Button/Button'
import Icon from 'semantic-ui-react/dist/commonjs/elements/Icon/Icon'
import Input from 'semantic-ui-react/dist/commonjs/elements/Input/Input'
import Label from 'semantic-ui-react/dist/commonjs/elements/Label/Label'
import Divider from 'semantic-ui-react/dist/commonjs/elements/Divider/Divider'
import Segment from 'semantic-ui-react/dist/commonjs/elements/Segment/Segment'
import Modal from 'semantic-ui-react/dist/commonjs/modules/Modal/Modal'
import Transition from 'semantic-ui-react/dist/commonjs/modules/Transition/Transition'
import Header from 'semantic-ui-react/dist/commonjs/elements/Header/Header'

class SendEmailForm extends PureComponent {
  state = {ccVisible: false, bccVisible: false}

  handleCloseSuccess = () => {window.location.reload()}
  handleCloseNoSuccess = () => {window.location.href = "https://www.siteminder.com"}
  handleServerSuccessClose = () => {window.location.reload()}
  ccToggleVisibility = () => {this.setState({ ccVisible: !this.state.ccVisible })}
  bccToggleVisibility = () => this.setState({ bccVisible: !this.state.bccVisible })

  render() {
    const { values , handleSubmit, handleChange, errors, touched, isSubmitting } = this.props
    const { ccVisible, bccVisible } = this.state

    return (
      <Fragment>
        <style jsx>{`
          #form-container {
            display: flex;
            justify-content: center;
          }
          `}</style>
          <div id='form-container'>
            <Segment stacked raised inverted size='small' color='blue'>
              <Label as='a' color='olive' ribbon> From: no-reply@mail.com </Label>
                <Header as='h1' textAlign='center'>
                  <Icon color ='olive' size='large' name='send' />
                  Free Mailer
                </Header>
                <Form size='small' onSubmit={handleSubmit}>
                  <Form.Input type='hidden' name ='_csrf' value={'TODO'} />
                  {touched.to && errors.to ?
                    <Message color='red' icon>
                      <Icon name='spy' color='black' />
                      <Message.Content>
                        <Message.Header>{`${errors.to}`}</Message.Header>
                      </Message.Content>
                  </Message>  : null}
                  <Form.Input label='To' autoComplete='text'
                              required name ='to' placeholder='test@example.com' onChange={handleChange} />
                  {ccVisible ? null : <label style={{cursor: 'pointer', color: '#b5cc18'}} onClick={this.ccToggleVisibility}> Cc</label>}
                  {ccVisible ? null : <label> / </label>}
                  <Transition visible={ccVisible} animation='scale' duration={500}>
                    <Form.Input label='Cc' autoComplete='text' name ='cc' onChange={handleChange} />
                  </Transition>
                  {bccVisible ? null :<label style={{cursor: 'pointer', color: '#b5cc18'}} onClick={this.bccToggleVisibility}> Bcc</label>}
                  <Transition visible={bccVisible} animation='scale' duration={500}>
                    <Form.Input label='Bcc' autoComplete='text' name ='bcc' onChange={handleChange} />
                  </Transition>
                  <Divider />
                  {touched.subject && errors.subject ?
                    <Message color='red' icon>
                      <Icon name='spy' color='black'/>
                      <Message.Content>
                        <Message.Header>{`${errors.subject}`}</Message.Header>
                      </Message.Content>
                    </Message>  : null}
                  <Form.Input type='text' label='Subject' autoComplete='text' required name ='subject' onChange={handleChange} />
                  {touched.content && errors.content ?
                    <Message color='red' icon>
                      <Icon name='spy' color='black'/>
                      <Message.Content>
                        <Message.Header>{`${errors.content}`}</Message.Header>
                      </Message.Content>
                    </Message>  : null}
                  <Form.TextArea label='Content' required name='content' onChange={handleChange} />
                  <Form.Field>
                    <Message color = 'olive' icon>
                      <Icon name='spy' color='black' />
                      <Message.Content>
                        <Header color='grey'>Guidelines</Header>
                        * To, Subject and Content are required & no duplicate emails allowed
                        <br/>
                        * Name and email address e.g John {`<john@ex.com>`} is not yet supported
                        <br/>
                        * To/Cc/Bcc multiple pattern eg. a@ex.com, b@xe.com, c@exe.com
                      </Message.Content>
                    </Message>
                  <label style={{color: 'white'}}>By using this service, I fully accept Mail's disclaimers and terms of use policy.</label>
                    <Button animated type='submit' color='olive' disabled={isSubmitting} >
                      <Button.Content visible>Send</Button.Content>
                      <Button.Content hidden>
                        <Icon name='send' />
                      </Button.Content>
                    </Button>
                    {isSubmitting ? <Icon loading inverted name='plane' color='grey' size='big' />: null}
                  </Form.Field>
                  <Modal style={{color: '#e0e0e0', fontSize: '1.5em'}} basic open={values.success}>
                    <Header color='olive' icon='checkmark' content='Successful delivery' />
                    <Modal.Content>
                      <p>Your email has been successfully delivered.
                      <span> Still fancy sending another one ? </span> </p>
                    </Modal.Content>
                    <Modal.Actions>
                      <Button color='olive' onClick={this.handleCloseSuccess}>
                        <Icon name='checkmark' /> Yes
                      </Button>
                      <Button color='blue' onClick={this.handleCloseNoSuccess}>
                        <Icon name='remove' /> No
                      </Button>
                    </Modal.Actions>
                  </Modal>
                  <Modal style={{color: '#e0e0e0', fontSize: '1.5em'}} basic open={!values.serverSuccess}>
                    <Header color='red' icon='remove' content={`${values.serverVal}`} />
                    <Modal.Actions>
                      <Button color='olive' onClick={this.handleServerSuccessClose}>
                        <Icon name='refresh' /> Try again
                      </Button>
                    </Modal.Actions>
                  </Modal>
                </Form>
            </Segment>
          </div>
      </Fragment>
    )
  }
}

// with HOC formik
const FormikSendEmail = withFormik({
  mapPropsToValues () {
    return {
      to: '',
      cc: '',
      bcc: '',
      subject: '',
      content: '',
      success: false,
      serverVal: '',
      serverSuccess: true
    }
  },

  validationSchema: Yup.object().shape({
    to: Yup.string().trim().required('To is required'),
    subject: Yup.string().trim().max(225, 'Error: Maximum 225 characters for the email subject.')
              .required('Subject is required'),
    content: Yup.string().trim().required('Content is required')
  }),

  handleSubmit (values, { resetForm, setErrors, setSubmitting, setValues}) {
    let to = values.to.trim()
    let cc = values.cc.trim()
    let bcc = values.bcc.trim()
    let subject = values.subject
    let content = values.content
    const successCode = [200, 202]
    const badRequest = [400]
    let err = false
    const emailPattern = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i
    const emailGroups = [to]
    const emailDupl = []
    if (cc) { emailGroups.push(cc) }
    if (bcc) { emailGroups.push(bcc) }
    for (let k = 0; k < emailGroups.length; k++) {
      if (emailGroups[k].includes(`,`)) {
        let emailGroupArr = emailGroups[k].split(`,`)
        for (let i = 0; i < emailGroupArr.length; i++) {
          let emailGroupItem = emailGroupArr[i].trim()
          emailDupl.push(emailGroupItem)
          if (!emailPattern.test(emailGroupItem)) {
            err = true
            break
          }
        }
      } else {
        let emailGroupItem = emailGroups[k].trim()
        emailDupl.push(emailGroupItem)
        if (!emailPattern.test(emailGroupItem)) {
          err = true
          break
        }
      }
    }
    // check duplicate email
    let duplEmails = emailDupl.reduce((list, item, index, array) => {
      if (array.indexOf(item, index + 1) !== -1 && list.indexOf(item) === -1) {
        list.push(item)
      }
      return list
    }, [])

    if (duplEmails.length) { err = true }

    // reject whitespaces
    if (subject.trim().replace(/\s/g, '').length < 1) {
      setErrors({subject: 'Error, Subject is required'})
    }

    if (content.trim().replace(/\s/g, '').length < 1) {
      setErrors({content: 'Error, Content is required'})
    }

    setTimeout(() => {
      try {
        if (!err) {
          const params = new URLSearchParams()
          params.append('to', to)
          params.append('subject', subject)
          cc ? params.append('cc', cc) : params.append('cc', ``)
          bcc ? params.append('bcc', bcc) : params.append('bcc', ``)
          content ? params.append('content', content) : params.append('content', ``)
          const client = axios.create({
            baseURL: 'http://localhost:9999',
            timeout: 3000,
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
          })

          // client.post(`/api/v1/mail/mailgun/send`, params)
          // client.post(`/api/v1/mail/sendgrid/send`, params)
          client.post(`/api/v1/mail/send`, params) // ok
          .then((res) => {
            if (successCode.includes(res.data.status)) {
              setValues({success: true, serverSuccess: true})
            } else {
               // From back-end
              if (badRequest.includes(res.data.status)) {
                setValues({serverSuccess: false,
                  serverVal: `Failed Delivery. Status: Invalid or duplicate email pattern (IODEP). Please read the guidelines and try again.`})
              } else {
                setValues({serverSuccess: false,
                  serverVal: `Failed Delivery. Status: ${res.data.statusText}. Please try again.`})
              }
              setSubmitting(false)
            }
          })
          .catch((e) => {
            setValues({serverSuccess: false,
              serverVal: `Failed Delivery. Status: ${e.message}. Please try again.`})
            setSubmitting(false)
          })
          resetForm(this.mapPropsToValues)
        } else {
          setErrors({to: `Invalid or duplicate email pattern. Please read the guidelines below`})
          setSubmitting(false)
        }
      } catch (e) {
        console.log(e)
        setSubmitting(false)
      }
    }, 2000) // 2 seconds
  }
})(SendEmailForm)

export default FormikSendEmail
