# FREE MAILER

## FREE MAILER SPECIFICATIONS:

    I. It has to accept the necessary information to send emails -- COMPLETED
        the necessary information is as follow:
        a. to -required - accept single or multiple email recipients
        b. From - sender address (can this be hardcoded such as default to no-reply@mail.com)
        c. CCs (optional) - accept single or multiple email recipients
        4. BCCs (optional) - accept single or multiple email recipients
        5. Email subject (required)
        6 Email body/content (required/required)
    II. It has modular back-end API abstractions connecting with at least 2 different email service providers with failover design -- COMPLETED
    III. Unit-tested App -- COMPLETED
    IV. No need to support HTML email body -- COMPLETED
    V. NO out-of-the-box email service provider SDKs or 3rd party email service client libraries is used -- COMPLETED
    VI. Responsive design and support most modern browsers -- COMPLETED
    VII. User friendly feedback in the events of errors/success -- COMPLETED
    VIII. Pleasant front-end UI -- COMPLETED

### DESIGN DECISIONS

    a. Fully responsive design, accessible from any device complying with Progressive Web Apps
    b. Automatic code splitting and minified JS bundle for fast performance courtesy of next.js
    c. Scalable and modular design for ease of maintainability and scalability
    d. SendGrid mail server is used as primary and MailGun mail server as the failover (as MailGun has domain restriction on the free acc) see the visual  design in free-mailer/EmailSenderDesign.jpg
    e. Developed with single responsibility and security principles in mind e.g only specific white-listed domains can access the back-end API.
    f. All user inputs are validated on both front-end and back-end
    g. Clean codebase with checking and linting by standardjs: the JavaScript Standard Style (https://standardjs.com)

### HOUSEKEEPING

a. The email sender/from is hardcoded to no-reply@mail.com

b. Both email subject and content are required as the rule is imposed by SendGrid  https://sendgrid.com/docs/API_Reference/api_v3.html

c. MailGun can only send emails to verified email addressess when using their default sandbox domain, unless your own domain is in use and you are a MailGun paying customer

### MAIN REQUIREMENTS

a. Node 6.12.0 or above

b. NPM

c. The internet

### HOW TO RUN THE UNIT TESTS

* JavaScript Testing Suite from Facebook - JEST is used

##### BACK-END

    a. cd back-end folder

    b. npm run test

### HOW TO SET UP AND RUN FREE MAILER VIA THE COMMAND LINE (LINUX/MAC)

Git clone this repository into your local machine https://github.com/raymondAntonio/free-mailer

Get into the cloned folder: cd free-mailer

##### BACK-END

a. cd back-end folder

b. Run npm install

c. Create config.env file in back-end folder's root e.g. back-end/config.env

    in the config.env file add the following:

    SG_APIKEY=<<YOUR SENDGRID API KEY>>

    MG_APIKEY=<<YOUR MAILGUN API KEY>>

    MG_DOMAIN=<<YOUR MAILGUN DOMAIN>>

    HOST=http://localhost:9999

    NODE_PORT=9999

d. Run the app: npm run dev, the default back-end API endpoint root runs on http://localhost:9999

##### FRONT-END

a. cd front-end folder

b. Run npm install

c. Run the app: npm run dev

d. Go to http://localhost:3000/mail. To use another port, you can run npm run dev -- -p <your port here>

e. Fill out the mail send form and then hit the send button.

f. Then please check your mail inbox AND spam inbox for the email you just sent if you send it to yourself for testing

### ROADMAP

a. Support for HTML email body type

b. Add attachment support

c. Add rich WYSIWYG HTML editor support

d. Add back-end API authentication

e. Add CSRF Protection once the back-end API authentication is done

f. Add recipient name and email support e.g John <john@ex.com>, Mary <mary@xe.com>

g. Add Recapcha

### TECH STACK

a. Node.js via Express.js for the back-end API abstraction

b. React.js via Next.js for the front-end 

c. Axios.js HTTP client for RESTful API communications on front-end and back-end


The demo app is hosted on AWS EC2 and publicly accessible at http://54.206.102.51:3000/mail
