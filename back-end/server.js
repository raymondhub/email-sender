`use strict`
const path = require(`path`)
require(`dotenv`).config({path: path.resolve(__dirname, 'config.env')})
const helmet = require(`helmet`)
const bodyParser = require(`body-parser`)
const express = require(`express`)
const app = express()
const port = process.env.NODE_PORT
const cors = require(`cors`)
// app.disable('x-powered-by')
app.use(helmet.hidePoweredBy({setTo: `Honey Pot 1.0`}))
app.use(helmet.noCache({noEtag: true})) // set Cache-Control header
app.use(helmet.noSniff())    // set X-Content-Type-Options header
app.use(helmet.frameguard()) // set X-Frame-Options header
app.use(helmet.xssFilter())  // set X-XSS-Protection header
// parse application/x-www-form-urlencoded
// allow rich object to true
app.use(bodyParser.urlencoded({extended: true}))
// parse application/json
app.use(bodyParser.json())
//app.use(cors())
// Comment out for dev testing if required
const whitelist = ['http://54.206.102.51:3000', 'http://localhost:3000']
const corsOptions = {
  origin: function (origin, callback) {
    if (whitelist.indexOf(origin) !== -1) {
      callback(null, true)
    } else {
      callback(new Error('401: Unauthorized Access Denied'))
    }
  }
}
app.use(cors(corsOptions))
app.listen(port, () => { console.log(`App listens on port ${port}`) })
require(`./app/routes`)(app)
