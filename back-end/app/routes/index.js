const emailRoutes = require('./email_routes')

module.exports = (app) => {
  emailRoutes(app)
}