'use strict'
const path = require('path')
require(`dotenv`).config({path: path.resolve(__dirname, '../../config.env')})
const safeJsonStringify = require('safe-json-stringify')
const axios = require('axios')
const querystring = require('querystring')
const emailPattern = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i
const emailFrom = `no-reply@mail.com`
const serverError = 500
const error = {}
error.status = 400
error.statusText = `Invalid Or Duplicate Email Pattern or missing required To/Subject/Content`
let duplEmailsCb = (list, item, index, array) => {
  if (array.indexOf(item, index + 1) !== -1 && list.indexOf(item) === -1) {
    list.push(item)
  }
  return list
}

let mGRecipients = (recipient) => {
  let recipientData = null
  const emailDupl = []
  if (recipient.includes(`,`)) {
    recipientData = []
    let recipientArrTmp = recipient.split(`,`)
    for (let i = 0; i < recipientArrTmp.length; i++) {
      let recipientArrTmpItem = recipientArrTmp[i].trim().replace(/\s/g, '')
      if (!emailPattern.test(recipientArrTmpItem)) {
        return error
      } else {
        emailDupl.push(recipientArrTmpItem)
      }
    }
  } else {
    let recipientItem = recipient.trim().replace(/\s/g, '')
    if (!emailPattern.test(recipientItem)) {
      return error
    } else {
      emailDupl.push(recipientItem)
    }
  }
  // check duplicate email
  let duplEmails = emailDupl.reduce(duplEmailsCb, [])
  if (duplEmails.length) {
    return error
  } else {
    recipientData = emailDupl.join()
    return recipientData
  }
}

let sGRecipients = (recipient) => {
  let recipientData = null
  const emailDupl = []
  if (recipient.includes(`,`)) {
    recipientData = []
    let recipientArrTmp = recipient.split(`,`)
    for (let i = 0; i < recipientArrTmp.length; i++) {
      let recipientArrTmpItem = recipientArrTmp[i].trim().replace(/\s/g, '')
      emailDupl.push(recipientArrTmpItem)
      if (!emailPattern.test(recipientArrTmpItem)) {
        return error
      } else {
        let recipientEach = {}
        recipientEach.email = `${recipientArrTmpItem}`
        recipientData.push(recipientEach)
      }
    }
  } else {
    let recipientItem = recipient.trim().replace(/\s/g, '')
    emailDupl.push(recipientItem)
    if (!emailPattern.test(recipientItem)) {
      return error
    } else {
      recipientData = recipient
    }
  }

  let duplEmails = emailDupl.reduce(duplEmailsCb, [])
  if (duplEmails.length) {
    return error
  } else {
    return recipientData
  }
}

let sGMailAssemblers = (to, subject, content, cc = ``, bcc = ``) => {
  let recipientsFinalObject = null
  let sGContentType = `text/plain`
  const toObj = {}
  if (to && !cc && !bcc) {
    let toSGRecipients = sGRecipients(to)
    if (typeof toSGRecipients === 'string') {
      const toEmail = {}
      toEmail.email = `${toSGRecipients}`
      toObj.to = [toEmail]
    } else {
      if (!Array.isArray(toSGRecipients)) {
        return toSGRecipients
      } else {
        toObj.to = toSGRecipients
      }
    }
    recipientsFinalObject = toObj
  }

  if (to && cc && !bcc) {
    let ccFinal = null
    let toFinal = null

    let toSGRecipients = sGRecipients(to)
    if (typeof toSGRecipients === 'string') {
      const toEmail = {}
      toEmail.email = `${toSGRecipients}`
      toFinal = [toEmail]
    } else {
      if (!Array.isArray(toSGRecipients)) {
        return toSGRecipients
      } else {
        toFinal = toSGRecipients
      }
    }

    let ccSGRecipients = sGRecipients(cc)
    if (typeof ccSGRecipients === 'string') {
      const ccEmail = {}
      ccEmail.email = `${ccSGRecipients}`
      ccFinal = [ccEmail]
    } else {
      if (!Array.isArray(ccSGRecipients)) {
        return ccSGRecipients
      } else {
        ccFinal = ccSGRecipients
      }
    }

    const toCcObj = {}
    toCcObj.to = toFinal
    toCcObj.cc = ccFinal
    recipientsFinalObject = toCcObj
  }

  if (to && !cc && bcc) {
    let bccFinal = null
    let toFinal = null

    let toSGRecipients = sGRecipients(to)
    if (typeof toSGRecipients === 'string') {
      const toEmail = {}
      toEmail.email = `${toSGRecipients}`
      toFinal = [toEmail]
    } else {
      if (!Array.isArray(toSGRecipients)) {
        return toSGRecipients
      } else {
        toFinal = toSGRecipients
      }
    }

    let bccSGRecipients = sGRecipients(bcc)
    if (typeof bccSGRecipients === 'string') {
      const bccEmail = {}
      bccEmail.email = `${bccSGRecipients}`
      bccFinal = [bccEmail]
    } else {
      if (!Array.isArray(bccSGRecipients)) {
        return bccSGRecipients
      } else {
        bccFinal = bccSGRecipients
      }
    }

    const toBccObj = {}
    toBccObj.to = toFinal
    toBccObj.bcc = bccFinal
    recipientsFinalObject = toBccObj
  }

  if (to && cc && bcc) {
    let toFinal = null
    let ccFinal = null
    let bccFinal = null

    let toSGRecipients = sGRecipients(to)
    if (typeof toSGRecipients === 'string') { // single
      const toEmail = {}
      toEmail.email = `${toSGRecipients}`
      toFinal = [toEmail]
    } else {
      if (!Array.isArray(toSGRecipients)) { // email pattern error
        return toSGRecipients
      } else { // multiple
        toFinal = toSGRecipients
      }
    }

    let ccSGRecipients = sGRecipients(cc)
    if (typeof ccSGRecipients === 'string') {
      const ccEmail = {}
      ccEmail.email = `${ccSGRecipients}`
      ccFinal = [ccEmail]
    } else {
      if (!Array.isArray(ccSGRecipients)) {
        return ccSGRecipients
      } else {
        ccFinal = ccSGRecipients
      }
    }

    let bccSGRecipients = sGRecipients(bcc)
    if (typeof bccSGRecipients === 'string') {
      const bccEmail = {}
      bccEmail.email = `${bccSGRecipients}`
      bccFinal = [bccEmail]
    } else {
      if (!Array.isArray(bccSGRecipients)) {
        return bccSGRecipients
      } else {
        bccFinal = bccSGRecipients
      }
    }

    const toCcBccObj = {}
    toCcBccObj.to = toFinal
    toCcBccObj.cc = ccFinal
    toCcBccObj.bcc = bccFinal
    recipientsFinalObject = toCcBccObj
  }

  let sGData = `{"personalizations": ${safeJsonStringify([recipientsFinalObject])},"from": {"email": "${emailFrom}"},"subject": "${subject}","content": [{"type": "${sGContentType}", "value": "${content}"}]}`

  return sGData
}

module.exports = (app) => {
  app.get(`/`, (req, res) => {
    res.send('Back-End Connection Test')
  })

  /*
    Main send mail API
  */
  app.post(`/api/v1/mail/send`, (req, res) => {
    const host = process.env.HOST
    const to = req.body.to
    const cc = req.body.cc
    const bcc = req.body.bcc
    const subject = req.body.subject
    const content = req.body.content
    const data = {}
    data[`from`] = `${emailFrom}`
    data[`to`] = `${to}`
    data[`cc`] = `${cc}`
    data[`bcc`] = `${bcc}`
    data[`subject`] = `${subject}`
    data[`content`] = `${content}`
    const clientHost = axios.create({
      baseURL: `${host}`,
      timeout: 3000,
      headers: {'Content-Type': 'application/x-www-form-urlencoded'}
    })
    clientHost.post(`/api/v1/mail/sendgrid/send`, querystring.stringify(data)) // SendGrid
    .then((response) => {
      const body = {}
      body.status = response.status
      body.statusText = response.statusText
      body.response = response
      // response.status = 500 //test mailgun failover
      if (response.status >= serverError) { // If over 499 means SendGrid server is somehow out of service then failover
        clientHost.post(`/api/v1/mail/mailgun/send`, querystring.stringify(data)) // MailGun
        .then((response) => {
          const body = {}
          body.status = response.status
          body.statusText = response.statusText
          body.response = response
          res.send(safeJsonStringify(body))
        })
        .catch((e) => {
          const error = {}
          error.status = e.response.status
          error.statusText = e.response.statusText
          error.response = e
          res.send(safeJsonStringify(error))
        })
      } else {
        res.send(safeJsonStringify(body))
      }
    })
    .catch((e) => {
      const error = {}
      error.status = e.response.status
      error.statusText = e.response.statusText
      // error.message = e.message
      error.response = e
      res.send(safeJsonStringify(error))
    })
  })

  /*
    MailGun back-end Abstraction
  */
  app.post(`/api/v1/mail/mailgun/send`, (req, res) => {
    const mGApiKey = process.env.MG_APIKEY
    const mGDomain = process.env.MG_DOMAIN
    const mGContentType = `text`
    let valError = false
    let to = req.body.to
    let cc = req.body.cc
    let bcc = req.body.bcc
    const subject = req.body.subject
    const content = req.body.content
    if (!subject || !content || !to) {
      valError = true
      res.send(safeJsonStringify(error))
    } else {
      let toMGRecipients = mGRecipients(to)
      if (typeof toMGRecipients === 'object') {
        valError = true
        res.send(safeJsonStringify(toMGRecipients))
      } else {
        to = toMGRecipients
      }
      if (cc) {
        let ccMGRecipients = mGRecipients(cc)
        if (typeof ccMGRecipients === 'object') {
          valError = true
          res.send(safeJsonStringify(ccMGRecipients))
        } else {
          cc = ccMGRecipients
        }
      }
      if (bcc) {
        let bccMGRecipients = mGRecipients(bcc)
        if (typeof bccMGRecipients === 'object') {
          valError = true
          res.send(safeJsonStringify(bccMGRecipients))
        } else {
          bcc = bccMGRecipients
        }
      }
    }

    const mGdata = {}
    mGdata[`from`] = `${emailFrom}`
    mGdata[`to`] = `${to}`
    if (cc) { mGdata[`cc`] = `${cc}` }
    if (bcc) { mGdata[`bcc`] = `${bcc}` }
    mGdata[`subject`] = `${subject}`
    mGdata[`${mGContentType}`] = `${content}`
    if (!valError) {
      axios({
        method: 'post',
        url: `https://api.mailgun.net/v3/${mGDomain}/messages`,
        timeout: 3000,
        auth: {
          username: 'api',
          password: `${mGApiKey}`
        },
        params: mGdata
      })
      .then((response) => {
        const body = {}
        body.status = response.status
        body.statusText = response.statusText
        body.response = response
        res.send(safeJsonStringify(body))
      })
      .catch((e) => {
        const error = {}
        error.status = e.response.status
        error.statusText = e.response.statusText
        // error.message = e.message
        error.response = e
        res.send(safeJsonStringify(error))
      })
    }
  })

  /*
    SendGrid back-end Abstraction
  */
  app.post(`/api/v1/mail/sendgrid/send`, (req, res) => {
    const sGApiKey = process.env.SG_APIKEY
    const to = req.body.to
    const cc = req.body.cc
    const bcc = req.body.bcc
    const subject = req.body.subject.replace(/"/g, '\\"')
    const content = req.body.content.replace(/"/g, '\\"')
    let sGMail = null
    if (!subject || !content || !to) {
      sGMail = error
    } else {
      sGMail = sGMailAssemblers(to, subject, content, cc, bcc)
    }
    if (typeof sGMail === 'string') {
      const sGClient = axios.create({
        baseURL: 'https://api.sendgrid.com/',
        timeout: 3000,
        headers: {'authorization': `Bearer ${sGApiKey}`,
          'content-type': 'application/json' }
      })

      sGClient.post(`/v3/mail/send`, sGMail)
        .then((response) => {
          const body = {}
          body.status = response.status
          body.statusText = response.statusText
          body.response = response
          res.send(safeJsonStringify(body))
        })
        .catch((e) => {
          const error = {}
          error.status = e.response.status
          error.statusText = e.response.statusText
          error.response = e
          res.send(safeJsonStringify(error))
        })
    } else {
      res.send(safeJsonStringify(sGMail))
    }
  })

  /*
    SendGrid Back-end Abstraction Mock for unit testing
    Reason:
    supertest request somehow can't connect the back-end and throws 500 server error
    because of this simple js replace(/"/g, '\\"') function.
    will file a bug report to supertest soon.
  */
  app.post(`/api/v1/mail/sendgrid/mock/send`, (req, res) => {
    const sGApiKey = process.env.SG_APIKEY
    const to = req.body.to
    const cc = req.body.cc
    const bcc = req.body.bcc
    // const subject = req.body.subject.replace(/"/g, '\\"')
    // const content = req.body.content.replace(/"/g, '\\"')
    const subject = req.body.subject
    const content = req.body.content
    let sGMail = null
    if (!subject || !content || !to) {
      sGMail = error
    } else {
      sGMail = sGMailAssemblers(to, subject, content, cc, bcc)
    }
    if (typeof sGMail === 'string') {
      const sGClient = axios.create({
        baseURL: 'https://api.sendgrid.com/',
        timeout: 3000,
        headers: {'authorization': `Bearer ${sGApiKey}`,
          'content-type': 'application/json' }
      })

      sGClient.post(`/v3/mail/send`, sGMail)
      .then((response) => {
        const body = {}
        body.status = response.status
        body.statusText = response.statusText
        body.response = response
        res.send(safeJsonStringify(body))
      })
      .catch((e) => {
        const error = {}
        error.status = e.response.status
        error.statusText = e.response.statusText
        error.response = e
        res.send(safeJsonStringify(error))
      })
    } else {
      res.send(safeJsonStringify(sGMail))
    }
  })
}
