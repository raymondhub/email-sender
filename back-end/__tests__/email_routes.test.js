'use strict'
const request = require('supertest')
const express = require(`express`)
const bodyParser = require(`body-parser`)
const app = express()
app.use(bodyParser.urlencoded({extended: true}))
app.use(bodyParser.json())
require(`../app/routes`)(app)

describe(`Test the root path -DONE way`, () => {
  test(`It should response the GET method`, (done) => {
    request(app).get('/').then((response) => {
      expect(response.statusCode).toBe(200)
      done()
    })
  })
})

describe(`Test the root path -PROMISE way`, () => {
  test(`It should response the GET method`, () => {
    return request(app).get('/').then(response => {
      expect(response.statusCode).toBe(200)
    })
  })
})

describe(`Test the root path -ASYNC way`, () => {
  test(`It should response the GET method`, async () => {
    const response = await request(app).get('/')
    expect(response.statusCode).toBe(200)
  })
})

describe(`Test the root path -SUPERTEST way`, () => {
  test(`It should response the GET method`, () => {
    return request(app).get('/').expect(200)
  })
})

describe(`Mailgun SEND Test`, () => {
  it(`The back-end connection should return 200 and validation should return 400 as 'TO' is empty`, (done) => {
    request(app)
      .post(`/api/v1/mail/mailgun/send`)
      .type(`form`)
      .send({to: ``, subject: `subject test`, content: `content test`})
      .set('Accept', 'application/x-www-form-urlencoded')
      .end((err, res) => {
        expect(res.status).toBe(200) // connection to back-end
        expect(res.text).toBe(`{"status":400,"statusText":"Invalid Or Duplicate Email Pattern or missing required To/Subject/Content"}`)
        done()
      })
  })
})

describe(`Mailgun SEND Test`, () => {
  it(`The back-end connection should return 200 and validation should return 400 as 'CC' is in invalid email pattern`, (done) => {
    request(app)
      .post(`/api/v1/mail/mailgun/send`)
      .type(`form`)
      .send({to: `raymond@aus.com.au`, cc: `validemail@dotcom`, subject: `subject test`, content: `content test`})
      .set('Accept', 'application/x-www-form-urlencoded')
      .end((err, res) => {
        expect(res.status).toBe(200) // connection to back-end
        expect(res.text).toBe(`{"status":400,"statusText":"Invalid Or Duplicate Email Pattern or missing required To/Subject/Content"}`)
        done()
      })
  })
})

describe(`Mailgun SEND Test`, () => {
  it(`The back-end connection should return 200 and validation should return 400 as 'Subject' is empty`, (done) => {
    request(app)
      .post(`/api/v1/mail/mailgun/send`)
      .type(`form`)
      .send({to: `john@example.com`, subject: ``, content: `content test`})
      .set('Accept', 'application/x-www-form-urlencoded')
      .end((err, res) => {
        expect(res.status).toBe(200) // connection to back-end
        expect(res.text).toBe(`{"status":400,"statusText":"Invalid Or Duplicate Email Pattern or missing required To/Subject/Content"}`)
        done()
      })
  })
})

describe(`SendGrid SEND Test`, () => {
  it(`The back-end connection should return 200 and validation should return 400 as 'Content' is empty`, (done) => {
    request(app)
    .post(`/api/v1/mail/sendgrid/mock/send`)
    .type(`form`)
    .send({to: `validEmail`, subject: `subject test`, content: ``})
    .set('Accept', 'application/x-www-form-urlencoded')
    .end((err, res) => {
      expect(res.status).toBe(200) // connection to back-end
      expect(res.text).toBe(`{"status":400,"statusText":"Invalid Or Duplicate Email Pattern or missing required To/Subject/Content"}`)
      done()
    })
  })
})

describe(`SendGrid SEND Test`, () => {
  it(`The back-end connection should return 200 and back-end validation should return status 400 as 'TO' is in invalid email pattern`, (done) => {
    request(app)
    .post(`/api/v1/mail/sendgrid/mock/send`)
    .type(`form`)
    .send({to: `validEmail`, subject: `subject test`, content: `content test`})
    .set('Accept', 'application/x-www-form-urlencoded')
    .end((err, res) => {
      expect(res.status).toBe(200) // connection to back-end
      expect(res.text).toBe(`{"status":400,"statusText":"Invalid Or Duplicate Email Pattern or missing required To/Subject/Content"}`)
      done()
    })
  })
})

describe(`SendGrid SEND Test`, () => {
  it(`The back-end connection should return 200 and back-end validation should return status 400 as 'BCC' is in invalid email pattern`, (done) => {
    request(app)
    .post(`/api/v1/mail/sendgrid/mock/send`)
    .type(`form`)
    .send({to: `smith@usa.com`, bcc: `validemail@dotcom`, subject: `subject test`, content: `content test`})
    .set('Accept', 'application/x-www-form-urlencoded')
    .end((err, res) => {
      expect(res.status).toBe(200) // connection to back-end
      expect(res.text).toBe(`{"status":400,"statusText":"Invalid Or Duplicate Email Pattern or missing required To/Subject/Content"}`)
      done()
    })
  })
})
